const ImagePlaceholder = () => {
  return (
    <div className="w-1/2 h-full">
      <img
        src="https://images.pexels.com/photos/37534/cube-six-gambling-play-37534.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
        className="w-full h-full object-cover"
        alt=""
      />
    </div>
  );
};

export default ImagePlaceholder;
