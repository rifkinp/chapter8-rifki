import React from "react";

const TitleH1 = props => {
  const {title1} = props;
  return (
    <h1 className="text-center text-mandarin text-2xl font-bold">{title1}</h1>
  );
};

export default TitleH1;
